/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lifegame;

import com.sun.javafx.beans.event.AbstractNotifyListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import myUIComponents.ValidationLabel;

/**
 *
 * @author Baka
 */
public class MainMenu extends AnchorPane
{
    private Stage mainStage;
    
    @FXML AnchorPane rootPanel;
    @FXML Button runButton;
    
    /**
     * LEFT-SIDE PANELS UI
     */
    
    @FXML ChoiceBox simTypeChoice;
    @FXML TextField gridSizeField;
    @FXML Slider randomRatioSld;
            
    // (grain-growth specyfic)
    
    @FXML Pane grainPanel;
    @FXML TextField randomRatioGrainField;
    @FXML ChoiceBox neightChoice;
    @FXML ChoiceBox grainChoice;
    @FXML RadioButton grain_perRadio;
    @FXML RadioButton grain_zeroSrcRadio;
    @FXML Slider grain_maxSpeedSlider;
    @FXML TextField probabilityField;
    
    @FXML CheckBox recrystal_checkbox;
    
    private Pane actualPane;
    public Scene menuScene;
    
    private Integer radiusValue = 1;
    private boolean continousRecrystal = false;
    private Integer probablityValue = 60;
    private SimulationType simType;
    
    public MainMenu(Stage mainStage)
    {
        this.mainStage = mainStage;
        
        FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("MainMenu.fxml"));
        
        fXMLLoader.setController(this);
        fXMLLoader.setRoot(this);
   
        try
        {
            fXMLLoader.load();
        }
        catch(IOException exc)
        {
            throw new RuntimeException(exc);
        }
        
        uiConfig();
    }
    
    private void uiConfig()
    {
        actualPane = grainPanel;
        
        /**
         * SIMTYPE FIELD
         */
        
        final ArrayList<String> simTypeArray = new ArrayList<>();
        simTypeArray.add("Basic grain growth");
        simTypeArray.add("Monte Carlo growth");
        simTypeArray.add("Dual Phase growth");
        simTypeArray.add("Recrystalization");
        
        ObservableList<String> simTypeList = new ObservableListBase<String>()
        {

            @Override
            public String get(int index)
            {
                return simTypeArray.get(index);
            }

            @Override
            public int size()
            {
                return simTypeArray.size();
            }
        };
        
        simTypeChoice.setItems(simTypeList);
        simTypeChoice.getSelectionModel().selectFirst();
        simTypeChoice.valueProperty().addListener(new ChangeListener()
        {

            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue)
            {
                switch((String)simTypeChoice.getValue())
                {
                    case "Basic grain growth":
                    {
                        simType = SimulationType.SimpleGrainGrowth;
                        
                        componentFade(true, randomRatioSld);
                        componentFade(false, randomRatioGrainField);   
                        
                        setCurrentPanel(grainPanel);
                        break;
                    }
                    case "Monte Carlo growth":
                    {
                        simType = SimulationType.MonteCarloGrowth;
                        componentFade(true, grainPanel);
                        break;
                    }
                    case "Dual Phase growth":
                    {
                        simType = SimulationType.DualPhaseGrowth;
                        componentFade(true, grainPanel);
                        break;
                    }
                    case "Recrystalization":
                    {
                        simType = SimulationType.Recrystalization;
                        componentFade(true, grainPanel);
                        break;
                    }
                }
            }
        });
       
        
        /**
         * GRIDSIZE FIELD
         */
        
        gridSizeField.setOnKeyTyped(new EventHandler<KeyEvent>()        // VALIDATION DOES NOT WORKS!
        {

            private void showError(String errorText)
            {
                ValidationLabel label = new ValidationLabel();
                label.setMessage(errorText, "red");
                label.showAsValidationTooltip(gridSizeField.getParent());
            }
            
            private void showAccept(String acceptText)
            {
                ValidationLabel label = new ValidationLabel();
                label.setMessage(acceptText, "value OK");
                label.showAsValidationTooltip(gridSizeField.getParent());
            }
            
            @Override
            public void handle(KeyEvent event)
            {
                if (event.getCharacter().codePointAt(0) >= 48 && event.getCharacter().codePointAt(0) <= 57)
                {
                    int fieldValue = 0;
                    try
                    {
                        fieldValue = Integer.parseInt(gridSizeField.getText());
                    }
                    catch(NumberFormatException exc)
                    {
                        gridSizeField.setText("");
                        showError("Ops! Wrong value entered");
                    }
                    
                    if (fieldValue > 10 && fieldValue < 900)
                        showAccept("Correct!");
                    else
                        showError("Value must be between 10 and 900");
                }
                else
                {
                    showError("Only numeric values are acceptable");
                    gridSizeField.setText("");
                }
           }
        });
        
        /**
         * RANDOMRATIO SLIDER
         */
        
        randomRatioSld.setLabelFormatter(new StringConverter<Double>()
        {

            @Override
            public String toString(Double object)
            {
                return object.intValue() + " %";
            }

            @Override
            public Double fromString(String string)
            {
                return Double.valueOf(string.substring(0, string.indexOf(" %")));
            }
        });
             
        /**
         * NEIGHTBOURCHOICE : GRAIN
         */
        
        ArrayList<String> neightChoiceList = new ArrayList<>();
        neightChoiceList.add("Von Neuman");
        neightChoiceList.add("Moore");
        neightChoiceList.add("Advanced");
        
        neightChoice.setItems(new ObservableListBase<String>()
        {

            @Override
            public String get(int index)
            {
                return neightChoiceList.get(index);
            }

            @Override
            public int size()
            {
                return neightChoiceList.size();
            }
        });
        
        neightChoice.setValue("Von Neuman");
        
        /**
         * GRAINCHOICE : GRAIN
         */

        ArrayList<String> grainChoiceList = new ArrayList<>();
        grainChoiceList.add("Random");
        grainChoiceList.add("Even");
        grainChoiceList.add("Radius");
        grainChoiceList.add("Manual selection");
        
        grainChoice.setItems(new ObservableListBase<String>()
        {

            @Override
            public String get(int index)
            {
                return grainChoiceList.get(index);
            }

            @Override
            public int size()
            {
                return grainChoiceList.size();
            }
        });
        
        grainChoice.setValue("Random");
          
        grainChoice.valueProperty().addListener(new ChangeListener()
        {

            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue)
            {
                if (newValue.equals("Radius"))
                {
                    TextInputDialog dialog = new TextInputDialog();
                    dialog.setTitle("Random radius");
                    dialog.setHeaderText("Random grain generation radius");
                    dialog.setContentText("Enter random radius (1 to grid size)");
                    
                    boolean valueValidate = false;
                    
                    while (!valueValidate)
                    {
                        Optional<String> radiusVal = dialog.showAndWait();
                        
                        try
                        {
                            radiusValue = Integer.valueOf(radiusVal.get());
                            if (!(radiusValue > 1 && radiusValue < 955))
                            {
                                dialog.setContentText("Enter proper value! (1 to grid size)");
                            }
                            else
                            {
                                valueValidate = true;
                            }
                        }
                        catch(NumberFormatException exc)
                        {
                            dialog.setContentText("Enter proper value! (1 to grid size)");
                        }
                    }

                }
            }
        });
        
        /**
         * RANDOMRATIOGRIDSLIDER : GRAIN
         */
        
        randomRatioGrainField.setTooltip(new Tooltip("Enter the number of grains which will" 
                + " be generated"));
        
        /**
         * SPEED SLIDER
         */
        
        StringConverter<Double> sliderConverter = new StringConverter<Double>()
        {

            @Override
            public String toString(Double object)
            {
                if (object > 1.00 && object < 59)
                {
                    return (object.intValue() - 1) + " FPS";
                }
                    
                    return object.intValue() + " FPS";
            }

            @Override
            public Double fromString(String string)
            {
                Double fpsVal = Double.valueOf(string.substring(0, string.indexOf(" FPS")));
                return 1000/(fpsVal-1);
            }
        };
        
        grain_maxSpeedSlider.setLabelFormatter(sliderConverter);
        
        /**
         * MONTECARLO AND RECRYSTAL CHECKBOXES : GRAIN
         */
        
        recrystal_checkbox.setOnMouseClicked(new EventHandler<MouseEvent>()
        {

            @Override
            public void handle(MouseEvent event)
            {
                if (recrystal_checkbox.isSelected())
                {
                    Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
                    dialog.setTitle("Question");
                    dialog.setHeaderText("Recrystallization type");
                    dialog.setContentText("Select the type of recrystallization :");

                    ButtonType btnSingle = new ButtonType("Single");
                    ButtonType btnConti = new ButtonType("Continous");
                    ButtonType btnCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

                    dialog.getButtonTypes().setAll(btnSingle, btnConti, btnCancel);
                    Optional<ButtonType> result = dialog.showAndWait();
                    if (result.get() == btnSingle)
                        continousRecrystal = false;
                    else if (result.get() == btnConti)
                        continousRecrystal = true;
                }
            }
        });
        
        neightChoice.valueProperty().addListener(new ChangeListener<String>()
        {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue)
            {
                if (newValue.equals("Advanced"))
                    probabilityField.setDisable(false);
                else
                    probabilityField.setDisable(true);
            }
        });
    }
    
    private void setCurrentPanel(Pane currentPane)
    {
        if (actualPane != currentPane)
        {
            currentPane.setOpacity(0);
            currentPane.setVisible(true);
            
            if (actualPane != null)
            {
                FadeTransition transition = new FadeTransition(Duration.millis(400), actualPane);
                transition.setFromValue(0.7);
                transition.setToValue(0);
                transition.setOnFinished(new EventHandler<ActionEvent>()
                {
                    @Override
                    public void handle(ActionEvent event)
                    { 
                        actualPane.setVisible(false);
                        
                        FadeTransition transition = new FadeTransition(Duration.millis(400), currentPane);
                        transition.setFromValue(0);
                        transition.setToValue(0.7);
                        transition.play();
                        
                        actualPane = currentPane;
                    }
                });
                transition.play();
            }
            else
            {
                FadeTransition transition = new FadeTransition(Duration.seconds(1), currentPane);
                transition.setFromValue(0);
                transition.setToValue(0.7);
                transition.play();
                
                actualPane = currentPane;
            }   
        }
    }
    
    private void componentFade(boolean visible, Node component)
    {
        FadeTransition transition = new FadeTransition(Duration.millis(400), component);
        double fromVal = 1.0;
        double endVal = 0;
        
        if (!visible)
        {
            fromVal = 0.0;
            endVal = 1.0;
            component.setOpacity(0.0);
            component.setVisible(true);
        }
        
        transition.setFromValue(fromVal);
        transition.setToValue(endVal);
        transition.setOnFinished(new EventHandler<ActionEvent>()
        {

            @Override
            public void handle(ActionEvent event)
            {
                Platform.runLater(new Runnable()
                {

                    @Override
                    public void run()
                    {
                        if (visible)
                            component.setVisible(false);
                    }
                });
            }
        });
        
        transition.play();
        

    }
    
    @FXML 
    public void onRunButtonClicked(MouseEvent evt)
    {
        String modeSelected = (String)simTypeChoice.getValue();
        
        int gridSize = 35;
                
        try
        {
            gridSize = Integer.parseInt(gridSizeField.getText());

            if (gridSize < 20 || gridSize > 955)
                throw new NumberFormatException();
        }
        catch(NumberFormatException exc)
        {
            Dialog errDialog = new Alert(Alert.AlertType.WARNING);
            errDialog.setContentText("Please enter proper grid size value (20 - 955).");
            errDialog.show();
            return;
        }
        
        switch(modeSelected)
        {
            case "Monte Carlo growth":
            case "Recrystalization":
            case "Basic grain growth":
            case "Dual Phase growth":
            {            
                int bcSelected;
                if (grain_zeroSrcRadio.isSelected())
                    bcSelected = 0;
                else
                    bcSelected = 2;
                
                String neighModeSelected = "moore";
                try
                {
                    if (neightChoice.getValue().equals("Von Neuman"))
                        neighModeSelected = "neuman";
                    else if (neightChoice.getValue().equals("Advanced"))
                        neighModeSelected = "advanced";
                }
                catch(NullPointerException exc)
                {
                    Dialog errDialog = new Alert(Alert.AlertType.WARNING);
                    errDialog.setContentText("Please select neighbour mode first.");
                    errDialog.show();
                    return;
                }

                String randomModeSelected = "random";
                if (simType == SimulationType.SimpleGrainGrowth)
                {
                    try
                    {
                        if (grainChoice.getValue().equals("Even"))
                            randomModeSelected = "even";
                        else if (grainChoice.getValue().equals("Radius"))
                            randomModeSelected = "radius";
                        else if (grainChoice.getValue().equals("Manual selection"))
                            randomModeSelected = "manual";
                    }
                    catch(NullPointerException exc)
                    {
                        Dialog errDialog = new Alert(Alert.AlertType.WARNING);
                        errDialog.setContentText("Please select grain generation mode first.");
                        errDialog.show();
                        return;
                    }
                }
   
                int gridRatioCount = 0;
                
                try
                {
                    gridRatioCount = Integer.parseInt(randomRatioGrainField.getText());

                    if (gridRatioCount < 0 || gridRatioCount > gridSize)
                        throw new NumberFormatException();
                }
                catch(NumberFormatException exc)
                {
                    Dialog errDialog = new Alert(Alert.AlertType.WARNING);
                    errDialog.setContentText("Wrong random grain value (1 - " +
                            gridSize + "). Default value will be selected.");
                    errDialog.show();
                    gridRatioCount = 10;
                }
                
                int probabilityValue = 70;
                if (neighModeSelected.equals("Advanced"))
                {
                    try
                    {
                        probabilityValue = Integer.parseInt(probabilityField.getText());
                    }
                    catch (NumberFormatException exc)
                    {
                        System.err.println("* Wrong probability value *");
                    }
                    
                }
                
                grainGameController gameDesign = new grainGameController(neighModeSelected, randomModeSelected,
                        gridSize,bcSelected,(int)grain_maxSpeedSlider.getValue(), gridRatioCount, radiusValue,
                        recrystal_checkbox.isSelected(), simType, continousRecrystal, probabilityValue);
                
                final grainGameController staticRefGameDesign = gameDesign;
                
                Scene gameScene = new Scene(gameDesign);
                mainStage.setHeight(900);
                mainStage.setWidth(1200);
                mainStage.setScene(gameScene);
                mainStage.setFullScreen(true);
                mainStage.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>()
                {

                    @Override
                    public void handle(KeyEvent event)
                    {
                        if (event.getCode() == KeyCode.ESCAPE)
                        {
                            staticRefGameDesign.handleWindowClosing();
                                    
                            mainStage.setWidth(405);
                            mainStage.setHeight(720);
                            mainStage.setScene(menuScene);
                        }
                    }
                });
                
                break;
            }
        }
    }
}

enum SimulationType
{
    SimpleGrainGrowth,
    MonteCarloGrowth,
    DualPhaseGrowth,
    Recrystalization
}

