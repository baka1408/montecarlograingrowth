/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lifegame;

import javafx.scene.paint.Color;

/**
 *
 * @author Baka
 */
public class GrainCell
{
    public static double A_VAL = 86710969050178.5;
    public static double B_VAL = 9.41268203527779;
    public static double CRIT_VAL = 46842668.25;
    
    public GrainCell()
    {
        state = false;
        alreadyRecrystalised = false;
        grainID = "NO GRAIN";
        color = null;
        dislocationVal = 0.00;
        recrystalGeneraiton = 0;
        montecarloID = 0;
        boundaryEnergy = 2.0;
        type = Type.grain;
    }
    
    public void CreateCopy(GrainCell original)
    {
        this.state = original.state;
        this.grainID = original.grainID;
        this.color = original.color;
        this.dislocationVal = original.dislocationVal;
        this.alreadyRecrystalised = original.alreadyRecrystalised;
        this.recrystalGeneraiton = original.recrystalGeneraiton;
        this.type = original.type;
        this.boundaryEnergy = original.boundaryEnergy;
    }
    
    public void CreateMonteCopy(GrainCell original)
    {
        this.state = original.state;
        this.grainID = original.grainID;
        this.color = original.color;
        this.montecarloID = original.montecarloID;
        this.boundaryEnergy = original.boundaryEnergy;
    }
    
    public boolean state;
    public String grainID;
    public Color color;
    public double dislocationVal;
    public boolean alreadyRecrystalised;
    public int recrystalGeneraiton;
    public int montecarloID;
    public Type type;
    public double boundaryEnergy;
    
    public int iPos;
    public int jPos;
    
    public enum Type {grain, inclusion}
}
