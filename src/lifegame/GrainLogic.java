/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lifegame;

import java.awt.Point;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.IntConsumer;
import java.util.stream.Stream;
import javafx.scene.paint.Color;
import javafx.util.Pair;

/**
 *
 * @author Baka
 */
public class GrainLogic
{
    /**
     * NEIGHBOUR TYPES : "moore","neuman","advanced"
     * RANDOM TYPES : "random","even","radius","manual"
     */
    
    public ArrayList<GrainCell> avaiableGrains;
    public ArrayList<Point> radiusPoints;
    public ArrayList<GrainCell> selectedGrains;
    public ArrayList<GrainCell> monteGrains;
    
    public GrainCell[][] gameTable;
    public GrainCell[][] previousStepTable;
    public Integer size;
    public int periodicMode;
    public String neighType;
    public String randomType;
    public int radiusVal;
    public int randomChanceVal;
    
    public double iterationsElapsed;
    
    public double prevStepRecrystalValue;
    public double recrystalGlobalDelta;
    public double recrystalSingleDelta;
    
    public int fieldStartEnergy;
    public int boundaryStartEnergy;
    public int nucleonsRate;
    
    public boolean isRecrystalEnabled;
    public boolean isRecrystalContinous = false;
    public boolean isGrowthFinished = false;
    public boolean hasRecrystalStarted = false;
    
    public NucleationType nucleationType;
    
    public SimulationType simType;
    
    public enum InclusionType {circular, square}
    
    public enum NucleationType {Constant, Increasing, SiteSaturated}

    private final String aliveSign = "[ * ]";
    private final String deadSign = "[ X ]";
    
    private LogHandler logHandler;
    
    public GrainLogic(int size)
    {
        this.size = size;
        periodicMode = 0;
        
        generateTable();
    }

    private void generateTable()
    {   
        if(simType == SimulationType.MonteCarloGrowth)
            return;
        
        iterationsElapsed = 0.000;
        recrystalGlobalDelta = 0.0;
        recrystalSingleDelta = 0.0;
        
        gameTable = new GrainCell[size][size];
        avaiableGrains = new ArrayList<>();
        selectedGrains = new ArrayList<>();
        radiusPoints = new ArrayList<>();
        
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {
                gameTable[i][j] = new GrainCell();
            }
    }

    public void iterate()
    {        
        if (simType == SimulationType.MonteCarloGrowth)
        {
            monteCarloIterate();
            return;
        }
        else if (simType == SimulationType.Recrystalization && hasRecrystalStarted)
        {
            recrystalIterate();
            return;
        }
        
        if (logHandler == null && isRecrystalEnabled)
            logHandler = new LogHandler();
        
        GrainCell[][] nextStepArray = new GrainCell[size][size];
        previousStepTable = gameTable;
        
        if (isRecrystalEnabled)
        {  
            double thisStepGlobalValue = ((GrainCell.A_VAL / GrainCell.B_VAL) + (1 - (GrainCell.A_VAL / GrainCell.B_VAL)) * 
                Math.exp(-GrainCell.B_VAL * iterationsElapsed));
            
            if (iterationsElapsed == 0.00)
                prevStepRecrystalValue = thisStepGlobalValue;
            
            recrystalGlobalDelta = thisStepGlobalValue - prevStepRecrystalValue;
            recrystalSingleDelta = recrystalGlobalDelta / (300 * 300);
              
            prevStepRecrystalValue = thisStepGlobalValue;
        }
        iterationsElapsed += 0.001;         // Increasing time step value
        
        double dislocationSum = 0.0;
        
        for(int i=0; i<size; i++)
        {
            for (int j=0; j<size; j++)
            {
                if (!gameTable[i][j].state)
                {
                    nextStepArray[i][j] = makeSentence(constructLocalAreaArray(i,j));
                }
                else
                {
                    nextStepArray[i][j] = gameTable[i][j];
                    
                    if (isRecrystalEnabled && (isRecrystalContinous || !gameTable[i][j].alreadyRecrystalised))
                        nextStepArray[i][j] = checkRecrystalConditions(constructLocalAreaArray(i, j));
                    
                    dislocationSum = dislocationSum + nextStepArray[i][j].dislocationVal;
                }                    
            }
        }
        
        if (isRecrystalEnabled)
            logHandler.WriteStateToFile(iterationsElapsed, dislocationSum);
               
        gameTable = nextStepArray;
        
        if (checkIfFinished())
        {
            //System.out.println("*** Structure growth has finished ***");
            distributeEnergy();
            isGrowthFinished = true;
        }
    }
        
    public void monteCarloIterate()
    {
        ArrayList<GrainCell> randomList = new ArrayList<>();
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {
                GrainCell curCell = gameTable[i][j];
                if (gameTable[i][j].type == GrainCell.Type.inclusion)
                {
                   curCell = new GrainCell();
                   curCell.CreateCopy(gameTable[i][j]);
                   curCell.iPos = i;
                   curCell.jPos = j; 
                }
                randomList.add(curCell);
            }
                
        
        Collections.shuffle(randomList);
        
        GrainCell[][] nextStepArray = new GrainCell[size][size];
        
        for(int i=0; i<randomList.size(); i++)
        {
            int curI = randomList.get(i).iPos;
            int curJ = randomList.get(i).jPos;
            
            if (gameTable[curI][curJ].type == GrainCell.Type.inclusion)
                nextStepArray[curI][curJ] = gameTable[curI][curJ];
            else
                nextStepArray[curI][curJ] = makeMonteSentence(constructLocalAreaArray(curI,curJ));
        }
        
        gameTable = nextStepArray;
    }
    
    public void recrystalIterate()
    {
        if (nucleationType != NucleationType.SiteSaturated)
            distributeNucleons();
        
        GrainCell[][] nextStepArray = new GrainCell[size][size];
        ArrayList<GrainCell> randomList = new ArrayList<>();
        
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {
                randomList.add(gameTable[i][j]);
            }
        
        Collections.shuffle(randomList);
       
        for(int i=0; i<randomList.size(); i++)
        {
            int curI = randomList.get(i).iPos;
            int curJ = randomList.get(i).jPos;
            
            if (gameTable[curI][curJ].type == GrainCell.Type.inclusion || !checkIfBordered(curI, curJ))
                nextStepArray[curI][curJ] = gameTable[curI][curJ];
            else
                nextStepArray[curI][curJ] = makeMonteRecrystalSentence(constructLocalAreaArray(curI,curJ));
        }
        
        gameTable = nextStepArray;
    }

    private GrainCell[][] constructLocalAreaArray(int i, int j)
    {
        GrainCell[][] localArea = new GrainCell[3][3];
        
        for (int k=-1; k<2; k++)
        {
            for (int l=-1; l<2; l++)
            {
                localArea[1+k][1+l] = getCell(i+k, j+l);
            }
        }

        localArea[1][1] = getCell(i, j);
       
        if(simType == SimulationType.MonteCarloGrowth)
            return localArea;
        
        if (neighType.equals("neuman"))
        {
            localArea[0][0] = new GrainCell();
            localArea[0][2] = new GrainCell();
            localArea[2][0] = new GrainCell();
            localArea[2][2] = new GrainCell();
        }
        else if (neighType.equals("pentagonal") || neighType.equals("hexagonal"))
        {
            int cellsLeftCounter = 3;
          
            if (neighType.equals("hexagonal"))
                cellsLeftCounter = 2;
            
            Random rnd = new Random();
            Point p = new Point();
            ArrayList<Point> generatedList = new ArrayList<>();
            
            while (cellsLeftCounter > 0)
            {
                p = new Point();
                p.y = rnd.nextInt(3);
                p.x = rnd.nextInt(3);
                if (!(p.x == 1 && p.y == 1) && !generatedList.contains(p))
                {
                    generatedList.add(p);
                    localArea[p.y][p.x] = new GrainCell();
                    cellsLeftCounter--;
                }
            }
        }
        else if (neighType.equals("l-hexagonal"))
        {
            localArea[2][0] = new GrainCell();
            localArea[0][2] = new GrainCell();
        }
        else if (neighType.equals("r-hexagonal"))
        {
            localArea[0][0] = new GrainCell();
            localArea[2][2] = new GrainCell();
        }
        
        return localArea;
    }
    
    private GrainCell makeSentence(GrainCell[][] localArea)
    {
        Map<String,Integer> neighbourMap = getLocalCount(localArea);
             
        /**
         * STANDARD CONDITIONS CHECK
         */
        
        
        if(neighbourMap.size() > 0)
        {            
            ArrayList<String> topGrainsArray = new ArrayList<>();
            
            int maxCountValue = Collections.max(neighbourMap.values());
            int maxCountTmp = maxCountValue;
            
            boolean drawChange = false;
            
            if (neighType.equals("advanced"))
            {
                if (maxCountTmp < 5)
                {
                    GrainCell[][] closestArray = cloneLocalArray(localArea);
                    
                    closestArray[0][0] = new GrainCell();
                    closestArray[0][2] = new GrainCell();
                    closestArray[2][0] = new GrainCell();
                    closestArray[2][2] = new GrainCell();
                    
                    Map<String,Integer> closestNeighbourMap = getLocalCount(closestArray);
                    maxCountTmp = closestNeighbourMap.isEmpty() ? 0 : Collections.max(closestNeighbourMap.values());
                    
                    if (maxCountTmp < 3)
                    {
                        closestArray = cloneLocalArray(localArea);
                        closestArray[0][1] = new GrainCell();
                        closestArray[1][0] = new GrainCell();
                        closestArray[1][2] = new GrainCell();
                        closestArray[2][1] = new GrainCell();
                        
                        closestNeighbourMap = getLocalCount(closestArray);
                        maxCountTmp = closestNeighbourMap.isEmpty() ? 0 : Collections.max(closestNeighbourMap.values());
                        
                        if (maxCountTmp < 3)
                            drawChange = true;
                            
                        else
                        {
                            localArea = closestArray;
                            neighbourMap = closestNeighbourMap;
                            maxCountValue = maxCountTmp;
                        }
                    }
                    else
                    {
                        localArea = closestArray;
                        neighbourMap = closestNeighbourMap;
                        maxCountValue = maxCountTmp;
                    }
                    
                }
            }
            
            for (Map.Entry<String,Integer> entry : neighbourMap.entrySet())
            {
                if (entry.getValue() == maxCountValue)
                {
                    topGrainsArray.add(entry.getKey());
                }
            }
            
            String winnerID = "";
            
            if (topGrainsArray.size() == 1)
                winnerID = topGrainsArray.get(0);
            else if (topGrainsArray.isEmpty())
                return localArea[1][1];
            else
            {
                Random rnd = new Random();
                winnerID = topGrainsArray.get(rnd.nextInt(topGrainsArray.size() - 1));
            }
            
            for (int h=0; h<3; h++)
                for (int k=0; k<3; k++)
                {
                    if (winnerID.equals(localArea[h][k].grainID))
                    {
                        if (isRecrystalEnabled)
                        {
                            GrainCell copyCell = new GrainCell();
                            copyCell.CreateCopy(localArea[h][k]);
                            return copyCell;
                        }
                        else
                        {
                            Random rndGen = new Random();
                            if (drawChange && rndGen.nextInt(100) > randomChanceVal)
                                return localArea[1][1];
                            
                            return localArea[h][k];
                        }
                           
                    }
                        
                }    
        }
        else
        {
            return localArea[1][1];
        }
        
        return null;
    }
    
    private GrainCell makeMonteSentence(GrainCell[][] localArea)
    {
        ArrayList<GrainCell> neighbourList = new ArrayList<>();
        
        boolean isOnBorder = false;
        
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++) 
            {
                if (localArea[1][1].montecarloID != localArea[i][j].montecarloID && localArea[i][j].montecarloID != 0)
                    isOnBorder = true;
            }
        
        if (!isOnBorder)
            return localArea[1][1];
        
        /**
         * SUM PRIMAL ENERGY VALUE
         */
        
        double primalSum = 0;
        
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                if (!(i == 1 && j == 1) && localArea[i][j].montecarloID != -1 && localArea[i][j].montecarloID != 0 && (localArea[i][j].montecarloID != localArea[1][1].montecarloID) && localArea[i][j].type != GrainCell.Type.inclusion)
                {
                    primalSum++;
                    neighbourList.add(localArea[i][j]);
                }
                    
            }
        }
        
        primalSum = primalSum * localArea[1][1].boundaryEnergy;   // Grain boundary energy (0.1-1.0)
        
        /**
         * CHECH IF RANDOM SELECTED ENERGY IS LOWER THEN PRIMAL
         */
        
        if(neighbourList.size() > 0)
        { 
            Random randObj = new Random();
            GrainCell selectedGrain = neighbourList.get(randObj.nextInt(neighbourList.size()));
            
            if ((calculateMonteEnergy(localArea,selectedGrain.montecarloID) * selectedGrain.boundaryEnergy) - primalSum <= 0) 
                localArea[1][1].CreateMonteCopy(selectedGrain);
        }

        return localArea[1][1];
    }
    
    private GrainCell makeMonteRecrystalSentence(GrainCell[][] localArea)
    {
        ArrayList<GrainCell> neighbourList = new ArrayList<>();
        
        boolean isOnBorder = false;
        
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++) 
            {
                if (localArea[1][1].montecarloID != localArea[i][j].montecarloID && localArea[i][j].montecarloID != 0)
                    isOnBorder = true;
            }
        
        if (!isOnBorder)
            return localArea[1][1];
        
        /**
         * SUM PRIMAL ENERGY VALUE
         */
        
        double primalSum = 0;
        
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                if (!(i == 1 && j == 1) && localArea[i][j].montecarloID != -1 && localArea[i][j].montecarloID != 0 && (localArea[i][j].montecarloID != localArea[1][1].montecarloID) && localArea[i][j].type != GrainCell.Type.inclusion)
                {
                    primalSum++;
                    neighbourList.add(localArea[i][j]);
                }
                    
            }
        }
        
        primalSum = primalSum * localArea[1][1].boundaryEnergy;   // Grain boundary energy (0.1-1.0)
        
        /**
         * CHECH IF RANDOM SELECTED ENERGY IS LOWER THEN PRIMAL
         */
        
        if(neighbourList.size() > 0)
        { 
            Random randObj = new Random();
            GrainCell selectedGrain = neighbourList.get(randObj.nextInt(neighbourList.size()));
            
            if ((calculateMonteEnergy(localArea,selectedGrain.montecarloID) * selectedGrain.boundaryEnergy) - primalSum <= 0) 
                localArea[1][1].CreateMonteCopy(selectedGrain);
        }

        return localArea[1][1];
    }
    
    private GrainCell checkRecrystalConditions(GrainCell[][] localArea)
    {
        /**
         * RECRYSTALISATION CONDITIONS CHECK
         */
       
        /**
         * One of the neightbours has recrystalized in the previous step (or joined to recrystal grain group)
         */
        
        if (!localArea[1][1].alreadyRecrystalised || isRecrystalContinous)
        {
            boolean hasHighestDislocationVal = true;
            
            ArrayList<GrainCell> recrystalisedNeightbours = new ArrayList<>();

            for(int i=0; i<3; i++)
                for (int j=0; j<3; j++)
                {
                    if ((localArea[i][j].alreadyRecrystalised && !isRecrystalContinous) 
                            || localArea[i][j].recrystalGeneraiton > localArea[1][1].recrystalGeneraiton)
                    {
                        recrystalisedNeightbours.add(localArea[i][j]);
                    }

                    if (localArea[i][j].dislocationVal > localArea[1][1].dislocationVal)
                        hasHighestDislocationVal = false;
                }
            
            if (recrystalisedNeightbours.size() > 0 && hasHighestDislocationVal)
            {
                Random rndCheck = new Random();
                
                GrainCell retGrain = new GrainCell();
                retGrain.CreateCopy(recrystalisedNeightbours.get(rndCheck.nextInt(recrystalisedNeightbours.size())));
                retGrain.dislocationVal = 0.00;
                
                return retGrain;
                
            }
        }
        
        /**
         * Otherwise, check if current cell is on the border of the grain
         */
        
        boolean isOnBorder = false;

        for(int i=0; i<3; i++)
            for (int j=0; j<3; j++)
            {
                if (!(localArea[1][1].grainID.equals(localArea[i][j].grainID)) && localArea[i][j].state)
                {
                    isOnBorder = true;
                    break;
                }

            }

        /**
         * Gets dislocation value in current step and summarize it to a current value
         */
        
        localArea[1][1].dislocationVal += getDislocationValue(isOnBorder);
        
        
        /**
         * Creates new, recrystalized grain if given conditions were passed
         */
        
        if ((isRecrystalContinous || !localArea[1][1].alreadyRecrystalised) && localArea[1][1].dislocationVal >= GrainCell.CRIT_VAL && isOnBorder)
        {
            GrainCell recrystalGrain = new GrainCell();
            
            recrystalGrain.color = getRandomColor();
            recrystalGrain.grainID = "RECRYSTALISED (" + recrystalGrain.color.toString() + ")";
            recrystalGrain.state = true;
            recrystalGrain.alreadyRecrystalised = true;
            recrystalGrain.dislocationVal = 0.00;
            recrystalGrain.recrystalGeneraiton = localArea[1][1].recrystalGeneraiton + 1;

            return recrystalGrain;
        }
        
        return localArea[1][1];
    }
    
    /**
     * MISCELLANEOUS FUNCTIONS
     */
    
    public boolean randomizeCells(int randomCount)
    {
        if (simType == SimulationType.MonteCarloGrowth)
        {       
            int count = 1;
            Random random = new Random();
            
            monteGrains = new ArrayList<>();
            for (int i=0; i<randomCount; i++)
            {
                GrainCell currCell = new GrainCell();
                currCell.montecarloID = count;
                currCell.color = getRandomColor();
                currCell.state = true;
                currCell.boundaryEnergy = 3.0;
                //if (periodicMode == 0)
                //    currCell.boundaryEnergy = (random.nextInt(100) / 100.0);
                
                monteGrains.add(currCell);
                count++;
            }
            
            for (int i=0; i<size; i++)
                for(int j=0; j<size; j++)
                {
                    if (gameTable[i][j] != null && gameTable[i][j].type != GrainCell.Type.inclusion)
                        createRandomMonteGrain(i, j);
                }
                    
            return true;
        }
        
        Random randDim = new Random();
        
        if(randomType.equals("even"))
        {        
            int margin = (int)Math.sqrt((size * size) / randomCount);    
            
            int leftAlign = (size - (((int)(size / margin)) * margin)) / 2;
            int topAlgin = leftAlign;
            
            for(int i=topAlgin; i<size; i += margin)
            {
                for (int j=leftAlign; j<size; j += margin)
                {
                    if (randomCount <= 0)
                        break;
                    createRandomGrain(i, j);
                    
                    randomCount--;
                }
            }
        }
        else if (randomType.equals("radius"))
        {
            Point currentTry = null;
            boolean pointGood = true;
            long startTime = System.currentTimeMillis();
            
            while(randomCount > 0)
            {
                currentTry = new Point(randDim.nextInt(size), randDim.nextInt(size));
                pointGood = checkPointsDistance(radiusVal, currentTry);
                
                System.out.println("Point state : " + pointGood);
                
                if (pointGood)
                {
                    randomCount--;
                    radiusPoints.add(currentTry);
                    createRandomGrain(currentTry.y, currentTry.x);
                }
                
                if (System.currentTimeMillis() - startTime > 500)
                {
                    return false;
                }
            }
        }
        else
        {    
            while(randomCount > 0)
            {
                createRandomGrain(randDim.nextInt(size), randDim.nextInt(size));
                randomCount--;
            }
        }
        
        return true;
    }
    
    public void randomizeInclusions(int randomCount)
    {
        Random randGen = new Random();
        InclusionType type = InclusionType.circular;

        int drawsRemains = randomCount / 4;
        
        if (iterationsElapsed == 0)
        {
            while (drawsRemains > 0)
            {
                if (randGen.nextBoolean())
                    type = InclusionType.square;
                else
                    type = InclusionType.circular;
                
                createInclusionStructure(randGen.nextInt(size), randGen.nextInt(size), randGen.nextInt(5), type);
                drawsRemains--;
            }            
        }
        else
        {
            LinkedList<Integer[]> borderGrains = getBorderGrains();
            if (borderGrains.size() == 0)
                return;
            
            while (drawsRemains > 0)
            {
                Integer[] drawedGrain = borderGrains.get(randGen.nextInt(borderGrains.size()));
                if (drawedGrain == null)
                    break;
                
                if (randGen.nextBoolean())
                    type = InclusionType.square;
                else
                    type = InclusionType.circular;
                            
                createInclusionStructure(drawedGrain[0], drawedGrain[1], randGen.nextInt(5), type);
                drawsRemains--;
            } 
        }
    }
    
    public void loadCells(GrainCell[][] rawData)
    {
        size = rawData.length;
        clearTable();
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
            {
                if (rawData[i][j] != null)
                {
                    GrainCell tmpCell = rawData[i][j];
                    
                    if (tmpCell.color != Color.BLACK && !tmpCell.color.toString().equals("0x000000ff"))
                    {
                        tmpCell.grainID = "LOADED(" + tmpCell.color.toString() + ")";
                        tmpCell.state = true;
                    }
                    else
                    {
                        tmpCell.grainID = "INCLUSION(" + tmpCell.color.toString() + ")";
                        tmpCell.state = true;
                        tmpCell.type = GrainCell.Type.inclusion;
                    }
                                        
                    gameTable[i][j] = tmpCell;
                    
                    boolean newGrain = true;
                    for (GrainCell rootCell : avaiableGrains)
                    {
                        if (rootCell.color == tmpCell.color)
                        {
                            newGrain = false;
                            break;
                        }
                    }
                    if (newGrain)
                        avaiableGrains.add(tmpCell);
                }
                else
                    gameTable[i][j] = new GrainCell();
            }
    }
    
    public double calculateMonteEnergy(GrainCell[][] localArea, int centerID)
    {
        double sum = 0;
        
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                if (!(i == 1 && j == 1) && localArea[i][j].montecarloID != 0 && localArea[i][j].montecarloID != centerID && localArea[i][j].type != GrainCell.Type.inclusion)
                {
                    sum += 1;
                }
                    
            }
        }
        
        return sum;
    }
        
    public void distributeEnergy(GrainCell[][] tmpGameTable)
    {
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {
                if (checkIfBordered(i, j))
                    tmpGameTable[i][j].boundaryEnergy = 5.0;
                else
                    tmpGameTable[i][j].boundaryEnergy = 2.0;
            }
        
        printEnergyState(tmpGameTable);
    }
    
    public void distributeEnergy()
    {
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {
                GrainCell tmpCell = new GrainCell();
                tmpCell.CreateCopy(gameTable[i][j]);
                if (checkIfBordered(i, j))
                    tmpCell.boundaryEnergy = 5.0;
                gameTable[i][j] = tmpCell;
            }
    }
    
    public void distributeNucleons()
    {
        ArrayList<GrainCell> borderGrains = new ArrayList<>();
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {
                if (gameTable[i][j].boundaryEnergy == 5.0)
                {
                    gameTable[i][j].iPos = i;
                    gameTable[i][j].jPos = j;
                    borderGrains.add(gameTable[i][j]);
                }
                    
            }
        
        if (borderGrains.size() < nucleonsRate)
            nucleonsRate = borderGrains.size();
        
        if (nucleonsRate == 0)
            return;
        
        Collections.shuffle(borderGrains);
        
        for (int i=0; i<nucleonsRate; i++)
        {
            GrainCell oldCell = borderGrains.get(i);
            GrainCell monteCell = createRandomMonteGrain(oldCell.iPos, oldCell.jPos);
            gameTable[monteCell.iPos][monteCell.jPos] = monteCell;
        }
        
        if (nucleationType == NucleationType.Increasing)
            nucleonsRate = (int)(nucleonsRate * 1.5);
    }
    
    public void prepareForRecrystal(int recrystalCount)
    {
        for(int i=0; i<size; i++)
            for(int j=0; j<size; j++)
            {
               gameTable[i][j].montecarloID = -1;
               gameTable[i][j].iPos = i;
               gameTable[i][j].jPos = j;
            }
               
        
        int count = 1;
        monteGrains = new ArrayList<>();
        for (int i=0; i<recrystalCount; i++)
        {
            GrainCell currCell = new GrainCell();
            currCell.montecarloID = count;
            currCell.color = getRandomColor();
            currCell.state = true;
            currCell.boundaryEnergy = 0.0;

            monteGrains.add(currCell);
            count++;
        }
    }
        
    public GrainCell createRandomGrain(int i, int j)
    {
        if (gameTable[i][j] != null && gameTable[i][j].state)
            return gameTable[i][j];
        
        GrainCell randomCell = new GrainCell();
        randomCell.color = getRandomColor();
        randomCell.grainID = "RANDOMIZED (" + randomCell.color.toString() + ")";
        randomCell.state = true;

        gameTable[i][j] = randomCell;
        avaiableGrains.add(randomCell);
        
        return randomCell;
    }
    
    public GrainCell createRandomMonteGrain(int i, int j)
    {
        Random random = new Random();
        GrainCell selectedGrain = monteGrains.get(random.nextInt(monteGrains.size()));
        
        GrainCell createdGrain = new GrainCell();
        createdGrain.CreateMonteCopy(selectedGrain);
        createdGrain.iPos = i;
        createdGrain.jPos = j;
        
        gameTable[i][j] = createdGrain;
        avaiableGrains.add(createdGrain);
        
        return createdGrain;
    }
    
    public void createInclusionStructure(int i, int j, int radius, InclusionType type)
    {
        if (type == InclusionType.circular)
        {          
            GrainCell inclusionCell = new GrainCell();
            inclusionCell.type = GrainCell.Type.inclusion;
            inclusionCell.state = true;
            inclusionCell.color = Color.BLACK;
            inclusionCell.grainID = "INCLUSION";
            
            for (int y = -radius; y <= radius; y++)
                for (int x = -radius; x <= radius; x++)
                    if ((x * x) + (y * y) <= (radius * radius) && x+i >= 0 && y+j >= 0 && x+i<size && y+j < size)
                        gameTable[x+i][y+j] = inclusionCell;
            return;
            
        }
        else if (type == InclusionType.square)
        {            
            GrainCell inclusionCell = new GrainCell();
            inclusionCell.type = GrainCell.Type.inclusion;
            inclusionCell.state = true;
            inclusionCell.color = Color.BLACK;
            inclusionCell.grainID = "INCLUSION";
            
            for (int a=-radius; a < radius; a++)
                for (int b=-radius; b < radius; b++)
                {
                    int x = i + a;
                    int y = j + b;
                    
                    if (x >= size || x < 0 || y >= size || y < 0)
                        continue;
                    
                    gameTable[x][y] = inclusionCell;
                }
        }
    }
    
    public boolean checkPointsDistance(int radius, Point newPoint)
    {
        //boolean normalDistance = Math.sqrt((Math.pow(p1.x - p2.x, 2)) + Math.pow(p1.y - p2.y, 2)) < radius;
        for (int i=newPoint.y-radius; i<newPoint.y+radius; i++)
        {
            for (int j=newPoint.x-radius; j<newPoint.x+radius; j++)
            {
                if (getCell(i, j).state)
                    return false;
            }
        }
        
        return true;
    }
    
    public Color getRandomColor()
    {
        Random r = new Random();
        Color color = new Color(r.nextDouble(), r.nextDouble(), r.nextDouble(), 1.0);
        
        boolean colorUnique = true;
        while (!colorUnique)
        {
            colorUnique = true;
            for(GrainCell entry : avaiableGrains)
            {
                if (entry.color == color)
                {
                    colorUnique = false;
                    color = new Color(r.nextDouble(), r.nextDouble(), r.nextDouble(), 1.0);
                }
                    
            }
        }
        
        return color;
    }
    
    private LinkedList<Integer[]> getBorderGrains()
    {
        LinkedList<Integer[]> borderGrains = new LinkedList<>();
        
        for(int i=0; i<size; i++)
            for(int j=0; j<size; j++)
            {
                if (gameTable[i][j] == null || gameTable[i][j].state == false || gameTable[i][j].type == GrainCell.Type.inclusion)
                    continue;
                
                if (checkIfBordered(i, j))
                {
                    Integer[] position = new Integer[2];
                    position[0] = i;
                    position[1] = j;
                    borderGrains.add(position);
                }
            }
        
        return borderGrains;
    }
    
    public void clearAndLeaveBorders(Color color)
    {
        GrainCell[][] tmpTable = new GrainCell[size][size];
        avaiableGrains.clear();
        
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {                    
                if (gameTable[i][j] != null && checkIfBordered(i, j))
                {
                   GrainCell borderGrain = new GrainCell();
                   borderGrain.color =  gameTable[i][j].state ? (gameTable[i][j].type == GrainCell.Type.inclusion ? gameTable[i][j].color : color) : color;
                   borderGrain.state = true;
                   borderGrain.type = GrainCell.Type.inclusion;
                   borderGrain.grainID = "BORDER INCLUSION";
                   
                   avaiableGrains.add(borderGrain);
                   tmpTable[i][j] = borderGrain;
                }
                else
                    tmpTable[i][j] = new GrainCell();
            }
        
        gameTable = tmpTable;
    }
    
    private Map<String, Integer> getLocalCount(GrainCell[][] localArea)
    {
        Map<String,Integer> neighbourMap = new HashMap<>();
        
        /**
         * STANDARD CONDITIONS CHECK
         */
        
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                if (localArea[i][j].state && localArea[i][j].type == GrainCell.Type.grain)
                {
                    if (neighbourMap.containsKey(localArea[i][j].grainID))
                    {
                        Integer tmpVal = neighbourMap.get(localArea[i][j].grainID);
                        tmpVal++;
                        neighbourMap.replace(localArea[i][j].grainID, tmpVal);
                    }
                    else
                    {
                        neighbourMap.put(localArea[i][j].grainID, 1);
                    }
                        
                }
            }
        }
        
        return neighbourMap;
    }
    
    private LinkedList<GrainCell> getSelectedGrain(GrainCell grain)
    {
        LinkedList<GrainCell> grains = new LinkedList<>();
        
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
            {
                if ((simType == SimulationType.SimpleGrainGrowth && gameTable[i][j].grainID.equals(grain.grainID)) || (simType == SimulationType.MonteCarloGrowth && gameTable[i][j].montecarloID == grain.montecarloID))
                    grains.add(gameTable[i][j]);
            }
        
        return grains;
    }
    
    public void lockSelectedGrain(GrainCell grain)
    {
        LinkedList<GrainCell> grains = getSelectedGrain(grain);
        for (int i=0; i < grains.size(); i++)
            grains.get(i).type = GrainCell.Type.inclusion;
    }
    
    public void recolorSelectedGrain(GrainCell grain, Color newColor)
    {
        LinkedList<GrainCell> grains = getSelectedGrain(grain);
        for (int i=0; i < grains.size(); i++)
            grains.get(i).color = newColor;
    }
    
    public void selectGrain(int x, int y)
    {   
        if (gameTable[x][y] == null || !gameTable[x][y].state)
            return;
        
        LinkedList<GrainCell> sameGrains = getSelectedGrain(gameTable[x][y]);
        
        GrainCell originalGrain = new GrainCell();
        if (simType == SimulationType.SimpleGrainGrowth)
            originalGrain.CreateCopy(gameTable[x][y]);
        else if (simType == SimulationType.MonteCarloGrowth)
            originalGrain.CreateMonteCopy(gameTable[x][y]);
        
        selectedGrains.add(originalGrain);
        
        for (int i=0; i < sameGrains.size(); i++)
            sameGrains.get(i).color = sameGrains.get(i).color.brighter();
    }
    
    public String getGrainInfo(int x, int y)
    {
        String infoText = "";
        if (gameTable[x][y] == null || !gameTable[x][y].state)
        {
            infoText += "No grain at position (" + x + "," + y + ")";
            return infoText;
        }
        
        infoText += "Grain at position (" + x + "," + y + ") Saved as [" + gameTable[x][y].iPos + "," + gameTable[x][y].jPos +"]\n";
        infoText += gameTable[x][y].type == GrainCell.Type.grain ? "Type : Grain\n" : "Type : Inclusion or locked grain\n";
        infoText += "Color RGB : " + gameTable[x][y].color.getRed() + " | " + gameTable[x][y].color.getGreen() + " | " + gameTable[x][y].color.getBlue() + "\n";
        infoText += "ID : " + gameTable[x][y].grainID + "\n";
        infoText += "Energy : " + gameTable[x][y].boundaryEnergy;
        
        return infoText;
    }
    
    public void deselectAllGrains()
    {
        for (int i=0; i < selectedGrains.size(); i++)
        {
            LinkedList<GrainCell> sameGrains = getSelectedGrain(selectedGrains.get(i));
            for (int j=0; j < sameGrains.size(); j++)
                sameGrains.get(j).color = selectedGrains.get(i).color; 
        }
        
        selectedGrains.clear();
    }
    
    public void clearTable()
    {
        GrainCell emptyCell = new GrainCell();
        gameTable = new GrainCell[size][size];
        
        for(int i=0; i<size; i++)
            for(int j=0; j<size; j++)
                gameTable[i][j] = emptyCell;
        
        avaiableGrains = new ArrayList<>();
        iterationsElapsed = 0.0;
        prevStepRecrystalValue = 1.0;
        
        if (logHandler != null)
            logHandler.WriteLineToFile("*** GAME TABLE CLEARED ***");
    }
    
    public void clearTable(GrainCell[] cellsToPreserve)
    {
        GrainCell emptyCell = new GrainCell();
        GrainCell[][] newGameTable = new GrainCell[size][size];
        avaiableGrains = new ArrayList<>();
        
        for(int i=0; i<size; i++)
            for(int j=0; j<size; j++)
            {
                newGameTable[i][j] = emptyCell;
                
                for (GrainCell singleCell : cellsToPreserve)
                    if ((gameTable[i][j].grainID.equals(singleCell.grainID) && simType == SimulationType.SimpleGrainGrowth) || ((gameTable[i][j].montecarloID == singleCell.montecarloID) && simType == SimulationType.MonteCarloGrowth))
                    {
                        newGameTable[i][j] = gameTable[i][j];
                        avaiableGrains.add(gameTable[i][j]);
                        break;
                    }
            }
             
        gameTable = newGameTable;
        iterationsElapsed = 0.0;
        prevStepRecrystalValue = 1.0;
        
        if (logHandler != null)
            logHandler.WriteLineToFile("*** GAME TABLE CLEARED ***");
    }
    
    public void closeTable()
    {
        if (isRecrystalEnabled && logHandler != null)
            logHandler.CloseFile();
    }

    private int boolToInt(boolean value)
    {
        return value ? 1 : 0;
    }

    private void printTable()
    {
        System.out.println(" PRINTING GLOBAL TABLE : \n\n");

        for (int i=0; i<size; i++)
        {
            for (int j=0; j<size; j++)
            {
                if (gameTable[i][j].state)
                    System.out.print(aliveSign);
                else
                    System.out.print(deadSign);
            }
            System.out.println();
        }
    }

    private void printTable(GrainCell[][] table)
    {
        System.out.println(" PRINTING LOCAL TABLE : \n\n");
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                System.out.print(table[i][j].grainID + " ; ");
            }
            System.out.println();
        }
    }
    
    public void printEnergyState(GrainCell[][] table)
    {
        System.out.println("\n\n\n\n********ENERGY STATE*************"); 
        int onBorder = 0;
        for (int i=0; i<table.length; i++)
        {
            System.out.println();
            for (int j=0; j<table.length; j++)
            {
                String cellInfo = "|" + String.valueOf((int)table[i][j].boundaryEnergy);
                System.out.print(cellInfo);
                if (table[i][j].boundaryEnergy == 5.0)
                    onBorder++;
            }
        }
        
        System.out.println("\n\n*** On border : " + onBorder + " ***\n\n\n\n");    
    }
    
    /**
     * BC-SAFE GET AND SET FOR THE BOARD
     */
    
    public void setCell(int i, int j)
    {
        if (i < 0)
            i = size + i;
        if (i >= size)
            i = 0 + (i - size);
        if (j < 0)
            j = size + j;
        if (j >= size)
            j = 0 + (j - size);
        
        gameTable[i][j].state = true;
    }
    
    public GrainCell getCell(int i, int j)
    {
        if (periodicMode == 2)
        {
            if (i < 0)
                i = size + i;
            if (i >= size)
                i = 0 + (i - size);
            if (j < 0)
                j = size + j;
            if (j >= size)
                j = 0 + (j - size);
            
            return gameTable[i][j];
        }
        else if (periodicMode == 0)
        {
            if (i < 0)
                i = 0;
            if (i >= size)
                i = size - 1;
            if (j < 0)
                j = 0;
            if (j >= size)
                j = size - 1;
            
            return gameTable[i][j];
        }
       
        return null;
    }
    
    private boolean checkIfBordered(int x, int y)
    {     
        if (gameTable[x][y] == null || gameTable[x][y].type != GrainCell.Type.grain || !gameTable[x][y].state)
            return false;
        
        for(int i=-1; i<1; i++)
            for(int j=-1; j<1; j++)
            {
                if (i == 0 && j == 0)
                    continue;
                if (x+i < 0 || x+i >= size || y + j < 0 || y + j >=size)
                    continue;
                if (gameTable[x+i][y+j] != null && !gameTable[x][y].grainID.equals(gameTable[x+i][y+j].grainID))
                    return true;
                    
            }
        return false;
    }
    
    private boolean checkIfFinished()
    {
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
                if (gameTable[i][j] == null || gameTable[i][j].state == false)
                    return false;
        
        return true;
    }
    
    private double getDislocationValue(boolean isOnBorder)
    {
        double randRatio = 0.0;
        
        try
        {  
            if (isOnBorder)
                randRatio = ((new Random().nextInt(61)) + 120) / 100.00;
            else
                randRatio = ((new Random().nextInt(30))) / 100.00;
        }
        catch(IllegalArgumentException exc)
        {
            randRatio = 0;
        }
        
        return recrystalSingleDelta * randRatio;

    }
    
    private GrainCell[][] cloneLocalArray(GrainCell[][] original)
    {
        GrainCell[][] cloned = new GrainCell[3][3];
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
            {
                cloned[i][j] = new GrainCell();
                if (original[i][j].state)
                    cloned[i][j].CreateCopy(original[i][j]);
            }
                
        
        return cloned;        
    }
    
    private void multiThreadFunc()
    {
        //        Thread[] decomposeThread = new Thread[8];
        //        
        //        for(int i=0; i<8; i++)
        //        {
        //            final int iterationRef = i;
        //            final int decompFragment = randomList.size() / 8;
        //            
        //            final Object mutex = new Object();
        //            
        //            decomposeThread[i] = new Thread(new Runnable()
        //            {
        //                @Override
        //                public void run()
        //                {
        //                    for (int i= iterationRef * decompFragment; i < (iterationRef * decompFragment) + decompFragment; i++)
        //                    {
        //                        int curI = randomList.get(i).iPos;
        //                        int curJ = randomList.get(i).jPos;
        //            
        //                        GrainCell currentCell = makeMonteSentence(constructLocalAreaArray(curI,curJ));
        //                        synchronized(mutex)
        //                        {
        //                            nextStepArray[curI][curJ] = currentCell;
        //                        }
        //                    }
        //                }
        //            });
        //            
        //            decomposeThread[i].start();
        //        }
        //        
        //        for(int i=0; i<8; i++)
        //        {
        //            try
        //            {
        //                decomposeThread[i].join();  
        //            }
        //            catch(InterruptedException exc)
        //            {
        //                exc.printStackTrace();
        //            }
        //            
        //        }
    }
    
    private void TestAllCells()
    {
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++)
                if (gameTable[i][j] == null)
                {
                    System.err.println("NULL FOUND AT POSITION " + i + "," + j);
                    return;
                }
                    
        
        System.out.println("All cells are valid");
    }
    
}
